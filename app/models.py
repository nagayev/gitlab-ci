from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Blog(models.Model):
    mane = models.TextField(blank=True)
    slug = models.SlugField()
    description = models.TextField(blank=True)
    card_img = models.ImageField(upload_to='img/blog/', blank=True)
    content = RichTextUploadingField(blank=True)
    owner = models.ForeignKey('app.Profile', on_delete=models.DO_NOTHING, related_name='blog_posts')
    date = models.DateTimeField(auto_now_add=True, blank=False)
    draft = models.BooleanField(blank=True)


class Direction(models.Model):
    mane = models.TextField()
    slug = models.SlugField()
    description = models.TextField(blank=True)
    img = models.ImageField(upload_to='img/dirs/', blank=True)
    contest_file = models.FileField(upload_to='files/dirs/', blank=True)
    videos = models.TextField(blank=True)
    examiners = models.ManyToManyField('app.Profile', related_name='exam_directions')


class ContestWork(models.Model):
    profile = models.ForeignKey('app.Profile', on_delete=models.DO_NOTHING, related_name='works')
    direction = models.ForeignKey(Direction, related_name='contest_works', on_delete=models.DO_NOTHING)
    work_link = models.TextField(blank=True)
    work = models.FileField(upload_to='files/works/', blank=True)


class Content(models.Model):
    key = models.TextField()
    draft = models.BooleanField(blank=True)
    content = models.TextField(blank=True)


class Partner(models.Model):
    name = models.TextField(blank=True)
    img = models.ImageField(upload_to='img/partners/')
    link = models.URLField(blank=True)


class Event(models.Model):
    mane = models.TextField(blank=True)
    slug = models.SlugField()
    description = models.TextField(blank=True)
    card_img = models.ImageField(upload_to='img/events/', blank=True)
    background_img = models.ImageField(upload_to='img/events/', blank=True)
    content = RichTextUploadingField(blank=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    avatar = models.ImageField(upload_to='img/profiles/', blank=True)
    direction = models.ManyToManyField(Direction, related_name='profiles', blank=True)
    quote = models.TextField(blank=True)
    theme = models.TextField(blank=True)
    socials = models.TextField(blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
