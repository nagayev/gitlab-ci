from django.shortcuts import render

from app.models import *


def index(request):
    context = {
        'events': Event.objects.all(),
        'quotes': Profile.objects.filter(user=User.objects.filter(is_staff=True), quote__isnull=False),
        'blog': Blog.objects.filter(draft=False),
        'partners': Partner.objects.all(),
        'content': Content.objects.all()
    }
    return render(request, template_name='index.html', context=context)


def down(request):
    return render(request, template_name='layout/site_down.html')
