#!/usr/bin/env bash

cd ~/site1000ln
git pull
docker-compose up -d
docker exec -it site1000ln_mainapp_1 python manage.py makemigrations --settings site1000ln.production
docker exec -it site1000ln_mainapp_1 python manage.py migrate --settings site1000ln.production

